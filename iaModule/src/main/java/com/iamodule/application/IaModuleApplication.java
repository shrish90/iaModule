package com.iamodule.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.iammodule.*"})
public class IaModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(IaModuleApplication.class, args);
	}
}

