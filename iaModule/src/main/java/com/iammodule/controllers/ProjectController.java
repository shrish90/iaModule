package com.iammodule.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.iammodule.model.Project;
import com.iammodule.services.ProjectDetailsService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/projects")
public class ProjectController {

@Autowired ProjectDetailsService projectDetailsService;
	
@RequestMapping(value = "/view", method = RequestMethod.GET, headers = "Accept=application/json")
public ResponseEntity<List<Project>> projectDetails() {
	List<Project> projects = projectDetailsService.projectDetails();
	if(projects.isEmpty()){
		return new ResponseEntity<List<Project>>(HttpStatus.NO_CONTENT);
	}else {
		return new ResponseEntity<>(projects,HttpStatus.OK);
	}
}

@RequestMapping(value = "/create", method = RequestMethod.POST)
public ResponseEntity<Map<Object,Object>> createProject(@RequestBody Project project){
	String response = "SUCCESS";
	Integer responseInt = projectDetailsService.saveProject(project);
	if(responseInt != 1) {
		response = "ERROR";
	}	
	Map<Object,Object> responseMap = new HashMap<Object,Object>();
	responseMap.put("root", response);
	return new ResponseEntity<>(responseMap,HttpStatus.CREATED);	
}

@RequestMapping(value = "/search", method = RequestMethod.GET)
public ResponseEntity<List<Project>> findProjectDetails(@RequestParam String searchField,
		@RequestParam String searchKey){
	List<Project> projects = projectDetailsService.searchProject(searchField, searchKey);
	if(projects.isEmpty()) {
		return new ResponseEntity<List<Project>>(HttpStatus.NO_CONTENT);
	}else {
		return new ResponseEntity<>(projects,HttpStatus.OK);
	}
}

@RequestMapping(value = "/delete", method = RequestMethod.GET)
public ResponseEntity<Map<Object,Object>> deleteProjectDetails(@RequestParam Integer rtcId){
	String response = "SUCCESS";
	Integer responseInt = projectDetailsService.deleteProject(rtcId);
	if(responseInt != 1) {
		response = "ERROR";
	}
	Map<Object,Object> responseMap = new HashMap<Object,Object>();
	responseMap.put("root", response);
	return new ResponseEntity<>(responseMap,HttpStatus.OK);
}

@RequestMapping(value = "/update/{iaStatus}", method = RequestMethod.POST)
public ResponseEntity<Map<Object,Object>> updateProject(@RequestBody Project project, @PathVariable String iaStatus){
	String response = projectDetailsService.updateProjectDetails(project, iaStatus);
	Map<Object,Object> responseMap = new HashMap<Object,Object>();
	responseMap.put("root", response);
	return new ResponseEntity<>(responseMap,HttpStatus.OK);
}

@RequestMapping(value="/uploadExcel", method = RequestMethod.POST)
public ResponseEntity<Map<Object,Object>> uploadExcelData(@RequestBody MultipartFile file){
	String response = projectDetailsService.uploadExcel(file);
	Map<Object,Object> responseMap = new HashMap<>();
	responseMap.put("root",response);
	return new ResponseEntity<>(responseMap,HttpStatus.CREATED);
	}

@RequestMapping(value = "/getIAStatus", method = RequestMethod.GET)
public ResponseEntity<Map<Object,Object>> getIAStatus(@RequestParam Integer rtcId){
	
	String iaStatus = projectDetailsService.getIAStatus(rtcId);
	Map<Object,Object> responseMap = new HashMap<>();
	responseMap.put("root",iaStatus);
	return new ResponseEntity<>(responseMap,HttpStatus.OK);
}
}


