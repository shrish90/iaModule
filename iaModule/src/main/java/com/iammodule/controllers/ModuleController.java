package com.iammodule.controllers;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iammodule.model.ModuleDetails;
import com.iammodule.services.ModuleService;

@RestController
@CrossOrigin("*")
public class ModuleController {

@Autowired ModuleService moduleService;
@RequestMapping("/saveIaDetails")
public ResponseEntity<Map<String,String>> saveIaDetails(@RequestBody ModuleDetails moduleDetails) {
	Map<String,String> responseMap = new HashMap<String,String>();
	responseMap.put("root", moduleService.saveDetailsService(moduleDetails));
	return new ResponseEntity<>(responseMap,HttpStatus.CREATED);
}
@RequestMapping("/getIaDetails")
public ResponseEntity<Map<String,List<ModuleDetails>>> getIaDetails(){
	List<ModuleDetails> moduleDetails = moduleService.getDetailsService();
	Map<String,List<ModuleDetails>> responseMap = new HashMap<String,List<ModuleDetails>>();
	responseMap.put("root", moduleDetails);
	return new ResponseEntity<>(responseMap,HttpStatus.OK);
}
}
