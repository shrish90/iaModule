package com.iammodule.services.servicesImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.iammodule.dao.ProjectDetailsDAO;
import com.iammodule.model.Project;
import com.iammodule.services.ProjectDetailsService;

@Service
public class ProjectDetailsServiceImpl implements ProjectDetailsService{
	@Autowired
	ProjectDetailsDAO projectDetailsDao;
	
	@Override
	public List<Project> projectDetails() {
		return projectDetailsDao.projectDetails();
	}

	@Override
	public Integer saveProject(Project project) {
		if(projectDetailsDao.persistProject(project) == 1){
			return saveProject1(project);
		}else return 0;
	}
	
	@Override
	public Integer saveProject1(Project project) {
		return projectDetailsDao.persistProject1(project);
	}	

	@Override
	public List<Project> searchProject(String field, String searchParameter) {
		return projectDetailsDao.findProject(field, searchParameter);
	}

	@Override
	public Integer deleteProject(Integer rtcId) {
		return projectDetailsDao.deleteProject(rtcId);
	}

	@Override
	public String uploadExcel(MultipartFile file) {
		List<Project> projectList = new ArrayList<>();
		try {
			Workbook workbook = new XSSFWorkbook(file.getInputStream());
			Sheet firstSheet = workbook.getSheetAt(0);
	        Iterator<Row> iterator = firstSheet.iterator();
	        
	        while (iterator.hasNext()) {
	        	//Row heading = iterator.next();
	            Row nextRow = iterator.next();
	            Project projectObj = new Project();
	            Iterator<Cell> cellIterator = nextRow.cellIterator();
	            while (cellIterator.hasNext()) {
	                Cell nextCell = cellIterator.next();
	                if(nextRow.getRowNum()==0) {
	                	continue;
	                }else {
	                	
	                	int columnIndex = nextCell.getColumnIndex();
	                    switch (columnIndex) {
	                    case 0:
	                    	projectObj.setRtcId(((Double)nextCell.getNumericCellValue()).intValue());
	                        break;
	                    case 1:
	                    	projectObj.setpName(nextCell.getStringCellValue());
	                        break;
	                    case 2:
	                    	String cdnum = (nextCell.getNumericCellValue()+"");
	                        projectObj.setCdNumber(cdnum.substring(0,cdnum.lastIndexOf('.')));
	                        break;  
	                    case 3:
	                        projectObj.setPmanagerName(nextCell.getStringCellValue());
	                        break;
	                    case 4:
	                        projectObj.setPmanagerEmail(nextCell.getStringCellValue());
	                        break;
	                    case 5:
	                    	String pjcode = (nextCell.getNumericCellValue()+"");
	                        projectObj.setPjCode(pjcode.substring(0,pjcode.lastIndexOf('.')));
	                        break;
	                    case 6:
	                        projectObj.setCreationDate(nextCell.getStringCellValue());
	                        break;
	                    case 7:
	                    	projectObj.setSolDesigner(nextCell.getStringCellValue());
	                        break;
	                    case 8:
	                    	projectObj.setTestLead(nextCell.getStringCellValue());
	                    	break;
	                    case 9:
	                    	projectObj.setIaOwner(nextCell.getStringCellValue());
	                    	break;
	                    case 10:
	                    	projectObj.setDocumentsLink(nextCell.getStringCellValue());
	                    	break;
	                    case 11:
	                    	projectObj.setComments(nextCell.getStringCellValue());
	                    	break;
	                    }
	                    
	                }
	                
	                
	            }
	            if(projectObj.getRtcId() !=null)
	            projectList.add(projectObj);
	           
	        }
	         
	        workbook.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return projectDetailsDao.saveMultipleProjects(projectList);
	}

	@Override
	public String updateProjectDetails(Project project, String iaStatus) {
		updateIaStatus(project.getRtcId(), iaStatus);
		return projectDetailsDao.updateProjectDetails(project, iaStatus);
	}

	@Override
	public String updateIaStatus(Integer rtcId, String iaStatus) {
		return projectDetailsDao.updateIaStatus(rtcId, iaStatus);
	}

	@Override
	public String getIAStatus(Integer rtcId) {
		return projectDetailsDao.getIAStatus(rtcId);
	}

}
