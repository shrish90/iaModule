package com.iammodule.services.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iammodule.dao.SaveDetailsDao;
import com.iammodule.model.ModuleDetails;
import com.iammodule.services.ModuleService;

@Service
public class ModuleServiceImpl implements ModuleService{
	@Autowired SaveDetailsDao saveDetailsDao;
	@Override
	public String saveDetailsService(ModuleDetails moduleDetails) {
		return saveDetailsDao.saveIaDetails(moduleDetails);
	}
	@Override
	public List<ModuleDetails> getDetailsService() {
		return saveDetailsDao.getModuleDetails();
	}

}
