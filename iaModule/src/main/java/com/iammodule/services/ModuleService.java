package com.iammodule.services;

import java.util.List;

import com.iammodule.model.ModuleDetails;

public interface ModuleService {
public String saveDetailsService(ModuleDetails moduleDetails);
public List<ModuleDetails> getDetailsService();
}
