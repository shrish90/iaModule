package com.iammodule.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.iammodule.model.Project;

public interface ProjectDetailsService {
	public List<Project> projectDetails();
	public Integer saveProject(Project project);
	public Integer saveProject1(Project project);
	public List<Project> searchProject(String field,String searchParameter);
	public Integer deleteProject(Integer rtcId);
	public String uploadExcel(MultipartFile file);
	public String updateProjectDetails(Project project, String iaStatus);
	public String updateIaStatus(Integer rtcId, String iaStatus);
	public String getIAStatus(Integer rtcId);
}
