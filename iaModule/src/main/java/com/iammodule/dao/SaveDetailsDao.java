package com.iammodule.dao;

import java.util.List;

import com.iammodule.model.ModuleDetails;

public interface SaveDetailsDao {
public String saveIaDetails(ModuleDetails moduleDetails);
public List<ModuleDetails> getModuleDetails();
}
