package com.iammodule.dao;

import java.util.List;

import com.iammodule.model.Project;

public interface ProjectDetailsDAO {

public List<Project> projectDetails();
public Integer persistProject(Project project);
public Integer persistProject1(Project project);
public List<Project> findProject(String field,String searchParameter);
public Integer deleteProject(Integer rtcId);
public String saveMultipleProjects(List<Project> projects);
public String updateProjectDetails(Project project,  String iaStatus);
public String updateIaStatus(Integer rtcId, String iaStatus);
public String getIAStatus(Integer rtcId);
}
