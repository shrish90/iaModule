package com.iammodule.dao.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.iammodule.dao.ProjectDetailsDAO;
import com.iammodule.model.Project;

@Repository
public class ProjectDetailsDAOImpl implements ProjectDetailsDAO{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public static final String projectFetchQuery = "SELECT * FROM PROJECTS";
	
	public static final String projectInsertQuery = "INSERT INTO "
			+ "PROJECTS(RTC_ID,PNAME,CD_NUMBER,PMANAGER_NAME,PMANAGER_EMAIL,PJ_CODE,CREATION_DATE,SOL_DESIGNER,TEST_LEAD,IA_OWNER,DOCUMENTS_LINK,COMMENTS)"
			+ " values (?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String projectInsertQuery1 = "INSERT INTO "
			+ "IA_SUBMISSION(RTC_ID,IA_OWNER,IA_REQUEST_DATE,IA_SUBMISSION_DATE,IA_TYPE,IA_SUBMISSION_EFFORT,IA_STATUS)"
			+ " values (?,?,NULL,?,'INCREMENTAL','2','INITIATED')";
	
	public static final String projectDeleteQuery = "DELETE FROM PROJECTS WHERE RTC_ID = ?";
	
/*	public static final String projectSearchQuery = "SELECT * FROM PROJECTS WHERE RTC_ID LIKE ? OR "
			+ "PNAME LIKE ? OR CD_NUMBER LIKE ? OR PMANAGER_NAME LIKE ? OR PMANAGER_EMAIL LIKE ?"
			+ " OR PJ_CODE LIKE ? OR CREATION_DATE LIKE ?";*/
	
	public static final String projectSearchQuery = "SELECT * FROM PROJECTS WHERE ? LIKE ? ";
	
	public static final String projectUpdateQuery = "UPDATE PROJECTS SET PNAME = ?,CD_NUMBER= ?,PMANAGER_NAME = ?, "
			+ "PMANAGER_EMAIL = ?,PJ_CODE = ?,CREATION_DATE = ?,SOL_DESIGNER = ?,TEST_LEAD = ?,"
			+ "IA_OWNER = ?,DOCUMENTS_LINK = ?,COMMENTS = ? WHERE RTC_ID = ?";
	
	public static final String projectUpdateiaQuery = "UPDATE IA_SUBMISSION SET IA_OWNER = ?  WHERE RTC_ID = ?";	
	public static final String UpdateIAStatus = "UPDATE IA_SUBMISSION SET IA_STATUS = ?  WHERE RTC_ID = ?";	
	public static final String getIAStatus = "SELECT IA_STATUS FROM IA_SUBMISSION WHERE RTC_ID = ?";
	@Override
	public List<Project> projectDetails() {
		return jdbcTemplate.query(projectFetchQuery, new BeanPropertyRowMapper<Project>(Project.class));
	}
	
	@Override
	public Integer persistProject(Project project){
		return jdbcTemplate.update(projectInsertQuery, project.getRtcId(),project.getpName(),
				project.getCdNumber(),project.getPmanagerName(),project.getPmanagerEmail(),
				project.getPjCode(),project.getCreationDate(),project.getSolDesigner(),
				project.getTestLead(),project.getIaOwner(),project.getDocumentsLink(),project.getComments());	
	}

	@Override
	public Integer persistProject1(Project project){
		return jdbcTemplate.update(projectInsertQuery1, project.getRtcId(),project.getIaOwner(),project.getCreationDate());		
	}	

	@Override
	public List<Project> findProject(String field, String searchParameter) {
		return jdbcTemplate.query(projectSearchQuery,
				new BeanPropertyRowMapper<Project>(Project.class), "%"+searchParameter+"%");
	}

	@Override
	public Integer deleteProject(Integer rtcId) {
		return jdbcTemplate.update(projectDeleteQuery, rtcId);
	}

	@Override
	public String saveMultipleProjects(List<Project> projects) {
		for(Project project:projects) {
			jdbcTemplate.update(projectInsertQuery, project.getRtcId(),project.getpName(),
					project.getCdNumber(),project.getPmanagerName(),project.getPmanagerEmail(),
					project.getPjCode(),project.getCreationDate(),project.getSolDesigner(),
					project.getTestLead(),project.getIaOwner(),project.getDocumentsLink(),project.getComments());	

			jdbcTemplate.update(projectInsertQuery1, project.getRtcId(),project.getIaOwner(),project.getCreationDate());			
		}
		return "SUCCESS";
	}

	@Override
	public String updateProjectDetails(Project project, String iaStatus) {
		jdbcTemplate.update(projectUpdateQuery, project.getpName(),project.getCdNumber(),project.getPmanagerName(),project.getPmanagerEmail(),
				project.getPjCode(),project.getCreationDate(),project.getSolDesigner(),
				project.getTestLead(),project.getIaOwner(),project.getDocumentsLink(),project.getComments(),project.getRtcId());

		jdbcTemplate.update(projectUpdateiaQuery,project.getIaOwner(),project.getRtcId());
		
		return "SUCCESS";
	}

	@Override
	public String updateIaStatus(Integer rtcId, String iaStatus) {
		jdbcTemplate.update(UpdateIAStatus,iaStatus,rtcId);
		return null;
	}

	@Override
	public String getIAStatus(Integer rtcId) {
		return jdbcTemplate.queryForObject(getIAStatus, new Object[] {rtcId}, String.class);
	}
}
