package com.iammodule.dao.daoImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.iammodule.dao.SaveDetailsDao;
import com.iammodule.model.ModuleDetails;

@Repository
public class SaveDetailsDaoImpl implements SaveDetailsDao{
	@Autowired
	JdbcTemplate jdbctemplate;
	
	private final String Insertsql = "INSERT INTO IA_table "
			+ "(RTC_ID, APPLICATION_NAME, ANALYSIS, DESIGN, BUILD,"
			+ "SYS_TESTING,PRT,L2_EFFORT,PM_EFFORT,MISC_EFFORT)"
			+ " values(?,?,?,?,?,?,?,?,?,?) ";
	
	private final String FETCH_QUERY = "SELECT RTC_ID,APPLICATION_NAME,"
			+ "ANALYSIS,DESIGN,BUILD,SYS_TESTING,PRT,L2_EFFORT,"
			+ "PM_EFFORT,MISC_EFFORT FROM IA_table";
	@Override
	public String saveIaDetails(ModuleDetails moduleDetails) {
		String response = "SUCCESS";
		Integer updateResponse = jdbctemplate.update(Insertsql,moduleDetails.getRtc_id(),moduleDetails.getApplication_name(),
				moduleDetails.getAnalysis(),moduleDetails.getDesign(),moduleDetails.getBuild(),
				moduleDetails.getSystem_testing(),moduleDetails.getPrt(),moduleDetails.getL2_effort(),
				moduleDetails.getPm_effort(),moduleDetails.getMisc_effort());
		if(updateResponse != 1){
			response = "ERROR";
		}
		return response;
	}
	@Override
	public List<ModuleDetails> getModuleDetails() {
		List<Map<String,Object>> responseMap = jdbctemplate.queryForList(FETCH_QUERY);
		List<ModuleDetails> resp = new ArrayList<>();
		for(Map<String,Object> map: responseMap) {
			ModuleDetails moduleDetails = new ModuleDetails();
			moduleDetails.setRtc_id((Integer)map.get("RTC_ID"));
			moduleDetails.setApplication_name(map.get("APPLICATION_NAME")+"");
			moduleDetails.setAnalysis(map.get("ANALYSIS")+"");
			moduleDetails.setDesign(map.get("DESIGN")+"");
			moduleDetails.setBuild(map.get("BUILD")+"");
			moduleDetails.setSystem_testing(map.get("SYS_TESTING")+"");
			moduleDetails.setPrt(map.get("PRT")+"");
			moduleDetails.setL2_effort(map.get("L2_EFFORT")+"");
			moduleDetails.setPm_effort(map.get("PM_EFFORT")+"");
			moduleDetails.setMisc_effort(map.get("MISC_EFFORT")+"");
			resp.add(moduleDetails);
	}
		return resp;
	}
}
