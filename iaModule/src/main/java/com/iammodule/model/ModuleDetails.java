package com.iammodule.model;

import org.springframework.stereotype.Component;

@Component
public class ModuleDetails {
	
	Integer rtc_id;
	String application_name;
	String analysis;
	String design;
	String build;
	String system_testing;
	String prt;
	String l2_effort;
	String pm_effort;
	String misc_effort;
	
	public Integer getRtc_id() {
		return rtc_id;
	}
	public void setRtc_id(Integer rtc_id) {
		this.rtc_id = rtc_id;
	}
	public String getApplication_name() {
		return application_name;
	}
	public void setApplication_name(String application_name) {
		this.application_name = application_name;
	}
	public String getAnalysis() {
		return analysis;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}
	public String getDesign() {
		return design;
	}
	public void setDesign(String design) {
		this.design = design;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getSystem_testing() {
		return system_testing;
	}
	public void setSystem_testing(String system_testing) {
		this.system_testing = system_testing;
	}
	public String getPrt() {
		return prt;
	}
	public void setPrt(String prt) {
		this.prt = prt;
	}
	public String getL2_effort() {
		return l2_effort;
	}
	public void setL2_effort(String l2_effort) {
		this.l2_effort = l2_effort;
	}
	public String getPm_effort() {
		return pm_effort;
	}
	public void setPm_effort(String pm_effort) {
		this.pm_effort = pm_effort;
	}
	public String getMisc_effort() {
		return misc_effort;
	}
	public void setMisc_effort(String misc_effort) {
		this.misc_effort = misc_effort;
	}

}
