package com.iammodule.model;

public class Project implements Cloneable {

    Integer rtcId;
    String pName;
	String cdNumber;
    String pmanagerName;
    String pmanagerEmail;
    String pjCode;
    String creationDate;
    String solDesigner;
    String testLead;
    String iaOwner;
    String documentsLink;
    String comments;
    
    public String getSolDesigner() {
		return solDesigner;
	}

	public void setSolDesigner(String solDesigner) {
		this.solDesigner = solDesigner;
	}

	public String getTestLead() {
		return testLead;
	}

	public void setTestLead(String testLead) {
		this.testLead = testLead;
	}

	public String getIaOwner() {
		return iaOwner;
	}

	public void setIaOwner(String iaOwner) {
		this.iaOwner = iaOwner;
	}

	public String getDocumentsLink() {
		return documentsLink;
	}

	public void setDocumentsLink(String documentsLink) {
		this.documentsLink = documentsLink;
	}

	public Integer getRtcId() {
        return rtcId;
    }

    public void setRtcId(Integer rtcId) {
        this.rtcId = rtcId;
    }
    
    public String getpName() {
 		return pName;
 	}

 	public void setpName(String pName) {
 		this.pName = pName;
 	}

 	
    public String getCdNumber() {
        return cdNumber;
    }

    public void setCdNumber(String cdNumber) {
        this.cdNumber = cdNumber;
    }
    
	public String getPmanagerName() {
		return pmanagerName;
	}

	public void setPmanagerName(String pmanagerName) {
		this.pmanagerName = pmanagerName;
	}

	public String getPmanagerEmail() {
		return pmanagerEmail;
	}

	public void setPmanagerEmail(String pmanagerEmail) {
		this.pmanagerEmail = pmanagerEmail;
	}

	public String getPjCode() {
        return pjCode;
    }

    public void setPjCode(String pjCode) {
        this.pjCode = pjCode;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }


	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
        
}
