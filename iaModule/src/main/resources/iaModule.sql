create database iaModuleDB;

CREATE TABLE iaModuleDB.IA_table(
  RTC_ID INTEGER NOT NULL,
  APPLICATION_NAME VARCHAR(45) NOT NULL,
  ANALYSIS VARCHAR(200),
  DESIGN VARCHAR(200),
  BUILD VARCHAR(200),
  SYS_TESTING VARCHAR(200),
  PRT VARCHAR(45),
  L2_EFFORT VARCHAR(45),
  PM_EFFORT VARCHAR(45),
  MISC_EFFORT VARCHAR(45));