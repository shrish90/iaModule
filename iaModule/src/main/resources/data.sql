-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PROJECTS`
--

DROP TABLE IF EXISTS `PROJECTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROJECTS` (
  `RTC_ID` int(5) DEFAULT NULL,
  `CD_NUMBER` varchar(10) DEFAULT NULL,
  `PNAME` varchar(100) DEFAULT NULL,
  `PMANAGER_NAME` varchar(50) DEFAULT NULL,
  `PMANAGER_EMAIL` varchar(50) DEFAULT NULL,
  `PJ_CODE` varchar(50) DEFAULT NULL,
  `SOL_DESIGNER` varchar(50) DEFAULT NULL,
  `TEST_LEAD` varchar(50) DEFAULT NULL,
  `IA_OWNER` varchar(50) DEFAULT NULL,
  `CREATION_DATE` varchar(19) DEFAULT NULL,
  `DOCUMENTS_LINK` varchar(200) DEFAULT NULL,
  `COMMENTS` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

use iaModuleDB;  
CREATE TABLE IA_SUBMISSION (
RTC_ID INT(5) NOT NULL,
IA_OWNER	VARCHAR(50),
IA_REQUEST_DATE	varchar(19),
IA_SUBMISSION_DATE	varchar(19),
IA_TYPE	VARCHAR(15),
IA_SUBMISSION_EFFORT INT(2) NOT NULL,
IA_STATUS	VARCHAR(20));
--
-- Dumping data for table `PROJECTS`
--

LOCK TABLES `PROJECTS` WRITE;
/*!40000 ALTER TABLE `PROJECTS` DISABLE KEYS */;
/*INSERT INTO `PROJECTS` VALUES (1002,'TEST','647920','Abhay','abhay.x.agarwa','jsjflslfh','2018-07-18 00:00:00'),(8687686,'hkhhk','hkhjkhkhkh','jkhkhkhkhjkh','jhkjhjkhkh@jfghfghfgh','jgggjgj','2018-07-18 00:00:00'),(7989799,'jhhjkhh','jkhkjkkgjfh','ddfd','gdfg@hkhkj','llljljl','2018-07-17 00:00:00'),(112344,'hgjgjg','jfjhgk','hgghfjfg','hgghfgh@ggjgh','ggghf','2018-07-18 00:00:00'),(765432,'hggjq','jjklhfds','sgjllhf','dfhjj@jklhg','dghh','2018-07-18 00:00:00'),(87543298,'jhkjjk','jfghddf',';jlhgjhfhgh','hjkgg@hgjhggk','gjhffg','2018-07-18 00:00:00'),(85498,'bnb','kgbkjb','hggk','bhhgk@jhk','jhu','2018-07-18 00:00:00'),(534534,'sfdfs','fsdfs','sfsdf','sfs@gsdgf','sfdfs','2018-07-18 00:00:00'),(56576,'kgjghjg','ffghfghfh','fghfghfhf','ghfghf@ffhgff','fhffhf','2018-07-19 00:00:00'),(123,'test','123','123','123@gmail.com','123','\"2018-07-21 00:00:0');
/*!40000 ALTER TABLE `PROJECTS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-28 17:06:58
