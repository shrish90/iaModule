var app = angular.module("iamodule",["ngRoute"]);

app.config(function($routeProvider) {
	  $routeProvider
	  .when("/", {
		    templateUrl : "module/project/index.html",
		    controller : "projectController"
		  })
		.when("/projectDetails", {
		    templateUrl : "module/project/index.html",
		    controller : "projectController"
		  })
	  .when("/iaDetails", {
	    templateUrl : "module/iamodule/index.html",
	    controller : "iamoduleController"
	  });
	});

