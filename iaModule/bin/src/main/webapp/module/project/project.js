app.directive("selectNgFiles", function() {
	  return {
		    require: "ngModel",
		    link: function postLink(scope,elem,attrs,ngModel) {
		      elem.on("change", function(e) {
		        var files = elem[0].files;
		        ngModel.$setViewValue(files);
		      })
		    }
		  }
		});
app.controller("projectController",function($scope,$http,$rootScope){
	var selectedRowToDelete;
	var updateProjectObj = {};
	$scope.selectedIndex = -1;
	$scope.editMode = false;
	$rootScope.selectedTab = 'projects';
	$scope.projectDetails = {};
	$(document).ready(function(){
	    $('.addProjectModal').modal();
	    $('.datepicker').datepicker({
	        container: 'body',
	        format: 'dd/mm/yyyy'
	      })
	    $('#deleteModal').modal();
	    $('.updateProjectModal').modal();
	    $('.dropdown-trigger').dropdown();
	  });
	$scope.initializeModal = function(){
		$scope.rtcId = "";
		$scope.projectName = "";
		$scope.cdNumber = "";
		$scope.pmName = "";
		$scope.pmEmail = "";
		$scope.pjCode = "";
		$scope.credate = "";
		$scope.soldesigner = "";
		$scope.testlead = "";
		$scope.iaowner= "";
		$scope.doclink = "";
		$scope.comments = "";
	};
	$scope.getProjectDetails = function(){
		$scope.projectDetails = {};
		$http.get("http://localhost:8080/projects/view").then(function(resp){
			$scope.projectDetails = resp.data;
		});
	};
	$scope.openModal = function(){
		$('.addProjectModal').modal('open');
	};
	$scope.submitProject = function(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd;
		} 
		if(mm<10){
		    mm='0'+mm;
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		var projObj = {
				"rtcId": $scope.rtcId,
				"pName": $scope.projectName,
				"cdNumber": $scope.cdNumber,
				"pmanagerName": $scope.pmName,
				"pmanagerEmail": $scope.pmEmail,
				"pjCode": $scope.pjCode,
				"creationDate": $scope.credate,
				"soldesigner": $scope.soldesigner,
				"testlead": $scope.testlead,
				"iaowner": $scope.iaowner,
				"doclink": $scope.doclink,
				"comments": $scope.comments
		};
		$http.post("http://localhost:8080/projects/create", projObj).then(function(resp){
			$scope.initializeModal();
			$scope.getProjectDetails();
			$('.addProjectModal').modal('close');	
		});
	};
	$scope.deleteThisEntry = function(rtcId){
		selectedRowToDelete = rtcId;
		$("#deleteModal").modal('open');
	};
	$scope.deleteRecord = function(){
		$http({
		    url: "http://localhost:8080/projects/delete", 
		    method: "GET",
		    params: {rtcId: selectedRowToDelete}
		 }).then(function(resp){
			 $scope.getProjectDetails();
			 $("#deleteModal").modal('close');
		 });
		
	};
	$scope.closeDeleteModal = function(){
		$("#deleteModal").modal('close');
	};
	$scope.uploadExcel = function(){
	
		var form = new FormData();
		form.append("file", $scope.excelFile[0]);

	        $http.post("http://localhost:8080/projects/uploadExcel", form, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        
	        }).then(function(resp){
	        	$scope.initializeModal();
				$scope.getProjectDetails();
				$('.addProjectModal').modal('close');	
	        });
	};
	/*shamala  */
	$scope.open1Modal = function(rowObj,index){
		console.log(rowObj);
		$('.updateProjectModal').modal('open');
		
		$scope.UpdatertcId = rowObj.rtcId;
		$scope.UpdatepName = rowObj.pName;
		$scope.UpdatecdNumber = rowObj.rtcId;
		$scope.UpdatepmanagerName = rowObj.pmanagerName;
		$scope.UpdatepmanagerEmail = rowObj.pmanagerEmail;
		$scope.UpdatepjCode = rowObj.pjCode;
		$scope.UpdatecreationDate = rowObj.creationDate;
		$scope.Updatesoldesigner = rowObj.solDesigner;
		$scope.Updatetestlead = rowObj.testLead;
		$scope.Updateiaowner = rowObj.iaOwner;
		$scope.Updatedoclink = rowObj.documentsLink;
		$scope.Updatecomments = rowObj.comments;
		
		
	};
	$scope.updateProject = function(index){
		$scope.selectedIndex = index;
		$scope.editMode = true;
	};
	
	$scope.getProjectDetails();	

	$scope.saveThisRow = function(projectObj){
		
		updateProjectObj = {
				"rtcId": $scope.UpdatertcId,
				"pName": $scope.UpdatepName,
				"cdNumber": $scope.UpdatecdNumber,
				"pmanagerName": $scope.UpdatepmanagerName,
				"pmanagerEmail": $scope.UpdatepmanagerEmail,
				"pjCode": $scope.UpdatepjCode,
				"creationDate": $scope.UpdatecreationDate,
				"solDesigner": $scope.Updatesoldesigner,
				"testLead": $scope.Updatetestlead,
				"iaOwner": $scope.Updateiaowner,
				"documentsLink": $scope.Updatedoclink,
				"comments": $scope.Updatecomments
		};
		
		$http.post("http://localhost:8080/projects/update", updateProjectObj).then(function(resp){			
			$scope.selectedIndex = -1;
			$scope.editMode = false;
			$scope.getProjectDetails();
			$('.updateProjectModal').modal('close');
		});
	};
	$scope.getProjectDetails();
});