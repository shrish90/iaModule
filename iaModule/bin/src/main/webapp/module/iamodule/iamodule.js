app.controller("iamoduleController",function($scope,$http,$rootScope){
	$rootScope.selectedTab = 'iaModule';
	$scope.showForm = false;
	$scope.details = {};
	$scope.moduleDetails = {};
	$scope.submitDisabled = true;
	$scope.showSuccess = false;
	$scope.submitDetails = function(){
		$scope.submitDisabled = true;
		$scope.details.rtc_id = $scope.rtc_id;
		$scope.details.application_name = $scope.applicationName;
		$scope.details.analysis = $scope.analysis;
		$scope.details.design = $scope.design;
		$scope.details.build = $scope.build;
		$scope.details.system_testing = $scope.st;
		$scope.details.prt = $scope.prt;
		$scope.details.l2_effort = $scope.l2_effort;
		$scope.details.pm_effort = $scope.pm_effort;
		$scope.details.misc_effort = $scope.misc_effort;
		$http.post("http://localhost:8080/saveIaDetails",$scope.details).then(
			       function(response){
			    	   $scope.details = {};
			    	   $scope.showSuccess = true;
				    	$scope.rtc_id = "";
						$scope.applicationName = "";
						$scope.analysis = "";
						$scope.design = "";
						$scope.build = "";
						$scope.st = "";
						$scope.prt = "";
						$scope.l2_effort = "";
						$scope.pm_effort = "";
						$scope.misc_effort = "";
						
			         });
	};
	
	$scope.onBlur = function(){
		$scope.showSuccess = false;
		if($scope.analysis && $scope.design && $scope.build && $scope.st){
			$scope.pm_effort = $scope.analysis + $scope.design + $scope.build + $scope.st;
		}else{
			$scope.pm_effort = "";
		}
		if($scope.l2_effort && $scope.prt){
			$scope.misc_effort = $scope.l2_effort + $scope.prt;
		}else{
			$scope.misc_effort = "";
		}
		if($scope.rtc_id && $scope.applicationName){
			$scope.submitDisabled = false;
		}else{
			$scope.submitDisabled = true;
		}
	};
	
	$scope.getModuleDetails = function(){
		$http.get("http://localhost:8080/getIaDetails").then(function(resp){
			$scope.moduleDetails = resp.data.root;
			//$scope.$apply();
		});
	};
	$scope.getModuleDetails();
});